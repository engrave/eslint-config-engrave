# @engrave/eslint-config-engrave

Common ESLint config for Engrave projects. This config is based on Airbnb's config with some modifications to fit our needs. It also includes Prettier rules to ensure code style consistency.

## Installation

Install the package together with eslint and prettier:

```bash
npm install --save-dev @engrave/eslint-config-engrave eslint prettier
```

## Config

Add the following to `.eslintrc` file:

```json
{
  "extends": "@engrave/eslint-config-engrave"
}
```

Add the following to `.prettierrc.js` file:

```javascript
module.exports = require('@engrave/eslint-config-engrave/prettier.config');
```

Add the following to `package.json` file:

```json
{
  "scripts": {
    "lint": "eslint . --ext .js,.jsx,.ts,.tsx"
  }
}
```

## License

MIT

