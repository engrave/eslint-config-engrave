module.exports = {
    extends: [
        "eslint:recommended",
        "plugin:security/recommended",
        "plugin:@typescript-eslint/recommended",
        "prettier",
    ],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        parser: '@typescript-eslint/parser',
        project: './tsconfig.json'
    },
    plugins: ["@typescript-eslint", "prettier", "no-only-tests", "import"],
    rules: {
        "@typescript-eslint/no-non-null-assertion": 0,
        "@typescript-eslint/no-explicit-any": 0,
        "security/detect-object-injection": 0,
        "security/detect-non-literal-fs-filename": 0,
        "@typescript-eslint/no-floating-promises": "error",
        "@typescript-eslint/no-unused-vars": [
            "error",
            {
                args: "all",
                destructuredArrayIgnorePattern: "^_",
                argsIgnorePattern: "^_",
                caughtErrors: "all"
            }
        ],
        "no-unused-vars": 0,
        "@typescript-eslint/naming-convention": [
            "warn",
            {
                selector: "enumMember",
                format: ["UPPER_CASE"],
            },
        ],
        "no-async-promise-executor": 0,
        "max-len": 0,
        "avoid-escape": 0,
        "prettier/prettier": 2,
        "no-only-tests/no-only-tests": ["error", { "fix": false }],
        "import/no-internal-modules": "off",
        "import/newline-after-import": ["error", { count: 1 }],
        "import/order": [
            // description: https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/order.md
            "error",
            {
                groups: [
                    "builtin",
                    "external",
                    "internal",
                    "parent",
                    "sibling",
                    "index",
                    "object",
                ],
                "newlines-between": "never",
                alphabetize: {
                    order: "asc",
                    caseInsensitive: true,
                },
            },
        ],
        "sort-imports": [
            "error",
            {
                ignoreCase: false,
                ignoreDeclarationSort: true,
                ignoreMemberSort: false,
                memberSyntaxSortOrder: ["none", "all", "single", "multiple"],
            },
        ],
    },
};
